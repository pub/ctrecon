# CTRecon

Test data hosted here: ftp://tmp.cba.mit.edu/Public/CTData/

## Tomopy

* https://tomopy.readthedocs.io/en/latest/ipynb/tomopy.html

* gridrec reconstruction algorithm working

  * assumes parallel beam (not cone-beam)
  * ~10 minutes for reconstruction of 750 projections of 1.5k x 1.9k pixels
  * jupyter notebook works for decimated data (x1/2, x1/4) but stalls on the full dataset, regular python is recommended for large datasets

* visualization using [ipyvolume](https://ipyvolume.readthedocs.io/en/latest/) in jupyter notebook


![coil_tomopy_recon](images/coil_tomopy_recon.gif)



To do:

* cone-beam reconstruction

  * [ASTRA toolbox](https://tomopy.readthedocs.io/en/latest/ipynb/astra.html) in tomopy
  * or DIY
    * cone to para: https://www.medphysics.wisc.edu/research/ct/publications/documents/Cone-to-Parallel.pdf
    * FDK is commonly used for 3D reconstruction which rebins the cone-beam data as a tilted fan projection
      * one step further would be to rebin from cone --> tilted fans --> parallel

* see if the reconstruction of the full data set is more reliable on a heftier workstation

  * C14/Carbon?
