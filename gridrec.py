
# coding: utf-8

# ## Gridrec
# 
# Here is an example on how to use [TomoPy](http://tomopy.readthedocs.io/en/latest/) with the gridrec algorithm.
# 
# [user manual](https://media.readthedocs.org/pdf/tomopy/latest/tomopy.pdf)

# In[3]:


# get_ipython().run_line_magic('pylab', 'inline')


# In[4]:


import tomopy
import dxchange
import numpy as np
import matplotlib.pyplot as plt
import ipyvolume as ipv


if __name__ == '__main__':

	haveLoaded = False

	if (haveLoaded):
		print("loading projection data...")
		proj = np.load("projection_data.npy")
	else:
		print("loading angle data...")
		ctdata = np.loadtxt("_data/test_scan_250/_ctdata.txt",skiprows=3)
		theta = np.deg2rad(ctdata[:,1])

		fname = '_data/test_scan_250/test scan 250_0001.tif'

		print("loading tiff stack...")
		indices = [i for i in range(1,752)]
		data = dxchange.read_tiff_stack(fname, indices)
		print(np.shape(data))

		# downsample and minus log
		print("downsampling and taking minus log...")
		proj = tomopy.minus_log(data[::1,::4,::4])
		print(np.shape(proj))

		print("saving projection data...")
		np.save('_output/projection_data',proj)

	rot_center = proj.shape[2]/2

	# gridrec
	print("running reconstruction...")
	rec = tomopy.recon(proj,theta,sinogram_order=False,
	                   center=rot_center,algorithm='art')

	# LPrec (not working)
	# rec = tomopy.recon(proj, theta, center=rot_center, 
	#                    algorithm=tomopy.lprec, lpmethod='lpfbp', 
	#                    filter_name='parzen')

	# ASTRA (not working)
	options = {'proj_type':'linear', 'method':'FBP'}
	rec = tomopy.recon(proj, theta, center=rot_center, 
	                     algorithm=tomopy.astra, options=options)

	print("applying circular mask...")
	rec = tomopy.circ_mask(rec, axis=0, ratio=0.75)

	print("saving data to reconstruction_data.npy...")
	np.save('_output/reconstruction_data',rec)

	# use jupyter notebook to visualize results

	# print("visualizing...")
	# ipv.figure()
	# vol = ipv.volshow(rec)
	# ipv.show()

